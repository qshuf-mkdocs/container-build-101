---
title: "Verify Setup"
weight: 2
---
Before we get started with the workshop, let us verify that the setup is up to the task by running some hello world examples of the packages we need.
