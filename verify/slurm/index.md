---
title: "Slurm"
weight: 3
---
Let's use Slurm's direct launcher after testing the `mpirun` launcher for our hello-world MPI script.

```bash
srun -n2 ${HOME}/hello
```

This will show similar output to the plain `mpirun`.

```bash
$ srun -n2 ${HOME}/hello
Hello world from processor compute1, rank 0 out of 2 processors
Hello world from processor compute2, rank 1 out of 2 processors
```
