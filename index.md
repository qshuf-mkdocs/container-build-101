---
title: "Home"
hide:
- navigation
- toc
---
# Building Container Images for HPC
Building container images for HPC application has its bells and whistles as an application aims to optimize for a given CPU, accelerator, and memory configuration. This workshop sheds light on the different aspects.
